<?php

require_once('/vendor/autoload.php');
require_once('/src/autoload.php');

$pug = new Pug(array(
	'basedir' => '/',
	'cache' => '/var/template_cache/'
));

function renderPage($title, $body) {
	global $pug;

	return $pug->render('/templates/main.pug', array(
		'title' => $title,
		'body' => $body
	));
}

$p = $_GET['__p'];

if (!isset($p)) {
	if (User::getCurrent() == null) {
		header('Location: /login');
		die();
	}

	echo 'Hello, ', User::getCurrent()->getName(), '! <a href="/logout">LOGOUT</a>';
} else if ($p == 'login') {
	$email = $_POST['email'];
	$pwd = $_POST['pwd'];

	$error = null;
	$success = null;

	if (isset($email) &&
		isset($pwd)) {

		try {
			$user = User::login($email, $pwd);
			header('Location: /');
			die();
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
	}

	die(renderPage('Login', $pug->render('/templates/login-form.pug', array(
		'error' => $error,
		'success' => $success
	))));
} else if ($p == 'logout') {
	User::logout();
	header('Location: /');
	die();
} else if ($p === 'register') {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$pwd = $_POST['pwd'];
	$pwd_confirm = $_POST['pwd_confirm'];
	$newsletter = $_POST['newsletter'] ? true : false;

	$error = null;
	$success = null;

	if (isset($name) &&
		isset($email) &&
		isset($pwd) &&
		isset($pwd_confirm) &&
		isset($newsletter)) {
		
		try {
			$user = User::register($name, $email, $pwd, $pwd_confirm, $newsletter);
			User::login($email, $pwd);
			header('Location: /');
			die();
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
	}

	die(renderPage('Register', $pug->render('/templates/register-form.pug', array(
		'error' => $error,
		'success' => $success
	))));
}