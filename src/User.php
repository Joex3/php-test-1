<?php

class User
{
    private static $db = null;

    static function getCurrent() : ?User
    {
        session_start();

        return isset($_SESSION['user_id']) ? new User($_SESSION['user_id']) : null;
    }

    static function logout() : bool
    {
        session_start();

        if (self::getCurrent() != null) {
            unset($_SESSION['user_id']);
            return true;
        } else {
            return false;
        }
    }

    static function login(string $email, string $password) : User
    {
        session_start();

        if (!isset($password)) {
            throw new Exception('No password provided.');
        }

        $user = self::getWithEmail($email);
        if ($user == null || !$user->checkPassword($password)) {
            throw new Exception('Invalid email or password.');
        }

        $_SESSION['user_id'] = $user->getId();

        return $user;
    }

    static function register(string $name, string $email, string $password, string $password_confirm, bool $newsletter) : User
    {
        if (!isset($name) || strlen($name) == 0) {
			throw new Exception('Invalid name.');
        }
        
        if (!isset($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
			throw new Exception('Invalid email.');
        }
        
        if (!isset($password) || $password !== $password_confirm) {
			throw new Exception('Passwords do not match.');
        }
        
        if (strlen($password) < 8) {
			throw new Exception('Password must be at least 8 characters long.');
		}

        $salt = self::generateSalt();
        $hash = self::hashPassword($password, $salt);

        if (strlen($salt) != 22 || strlen($hash) != 60) {
            throw new Exception('Internal server error.');
        }

        self::ensureInitialized();
        
        $stmt = self::$db->prepare('SELECT id FROM users where email = :email');
        $stmt->execute(array('email' => $email));
        if ($stmt->rowCount() != 0) {
            throw new Exception('Email is already in use.');
        }

        $stmt = self::$db->prepare('INSERT INTO users (name, email, password_hash, password_salt, newsletter) VALUES (:name, :email, :password_hash, :password_salt, :newsletter)');
        if (!$stmt->execute(array(
                'name' => $name,
                'email' => $email,
                'password_hash' => $hash,
                'password_salt' => $salt,
                'newsletter' => $newsletter ? 1 : 0
            ))) {
            throw new Exception('Internal server error.');
        }

        return new User((int)self::$db->lastInsertId());
    }

    static function getWithEmail(string $email) : ?User
    {
        if (!isset($email)) {
            throw new Exception('No email provided.');
        }

        self::ensureInitialized();

        $stmt = self::$db->prepare('SELECT id FROM users WHERE email = :email');
        if (!$stmt->execute(array('email' => $email))) {
            throw new Exception('Could not fetch user.');
        }

        $row = $stmt->fetch();
        if (!$row) {
            return null;
        } else {
            return new User((int)$row['id']);
        }
    }

    private static function ensureInitialized() : void
    {
        if (self::$db == null) {
            self::$db = new PDO('mysql:host=db;dbname=UserDatabase', 'root', 'root');
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }

    private static function generateSalt() : string
    {
        $salt = substr(base64_encode(openssl_random_pseudo_bytes(17)), 0, 22);
        $salt = str_replace('+', '.', $salt);
        return $salt;
    }
    
    private static function hashPassword(string $password, string $salt, int $cost = 7)
    {
        // TODO: Validate salt character range for blowfish algorithm.
        $param = '$' . implode('$', array('2y', str_pad($cost, 2, '0', STR_PAD_LEFT), $salt));
        return crypt($password, $param);
    }

    private $id;

    private function __construct(int $id)
    {
        $this->id = $id;
    }

    public function checkPassword(string $password) : bool
    {
        self::ensureInitialized();

        $stmt = self::$db->prepare('SELECT password_salt, password_hash FROM users WHERE id = :id');
        if (!$stmt->execute(array('id' => $this->id))) {
            throw new Exception('Could not fetch user.');
        }

        $row = $stmt->fetch();
        if (!$row) {
            throw new Exception('Could not fetch user.');
        }

        $salt = $row['password_salt'];
        $hash = $row['password_hash'];
        $input_hash = self::hashPassword($password, $salt);

        return hash_equals($hash, $input_hash);
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getName() : string
    {
        self::ensureInitialized();

        $stmt = self::$db->prepare('SELECT name FROM users WHERE id = :id');
        if (!$stmt->execute(array('id' => $this->id))) {
            throw new Exception('Could not fetch user.');
        }

        $row = $stmt->fetch();
        if (!$row) {
            throw new Exception('User does not exist.');
        }

        return $row['name'];
    }
}