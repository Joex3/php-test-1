# php-test-1

This example contains a simple login form with an attached database.

## Usage

First make sure you have [docker](https://www.docker.com/get-started) installed.

Then you need to download the dependencies using composer:
```sh
$ chmod +x composer.sh
$ ./composer.sh install
```
When asked to install optional dependencies respond with n.


Then start the server:
```sh
$ docker-compose up -d
```

To stop the server:
```sh
$ docker-compose down -v
```